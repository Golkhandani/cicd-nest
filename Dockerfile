FROM node:14.15.5-alpine
# create and set app directory
WORKDIR /usr/src/app/

# install app dependencies
# this is done before the following COPY command to take advantage of layer caching
COPY package*.json ./ 
RUN npm install

# copy app source to destination container
COPY . .

# expose container port
EXPOSE 3000

CMD npm run pm2

# docker build -t golkhandani/node-web-app .
# docker run -p 3000:3000 golkhandani/node-web-app